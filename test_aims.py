from nose.tools import assert_equal
from nose.tools import assert_almost_equal
import aims


def test_ints():
	obs = aims.std([1,2,3,4,5,6])
	exp = 1.7078251276599
	assert_almost_equal(obs,exp)

def test_negativeints():
	numbers = [-2,-2,-7,-9]
	obs = aims.std(numbers)
	exp = 3.0822070014845
	assert_almost_equal(obs,exp)

def test_float():
	obs = aims.std([-5.5,7.6,-11.5])
	exp =  7.9751001386955771
	assert_almost_equal(obs,exp)

def test_negativeints():
	numbers = [-4.7]
	obs = aims.std(numbers)
	exp = 0
	assert_equal(obs,exp)

def test_EmptyPath():
    obs = aims.avg_range([])
    exp = None
    assert_equal(obs, exp)

def test_paths_ints():
    obs = aims.avg_range(['data/bert/audioresult-00215','data/bert/audioresult-00412','data/bert/audioresult-00270'])
    exp = 5
    assert_equal(obs, exp)

def test_paths_floats():
    ob = aims.avg_range(['data/bert/audioresult-00215','data/bert/audioresult-00412','data/frank_richard/data_212'])
    exp = 5.0
    assert_equal(ob, exp)
