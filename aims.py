import numpy as np

def main():
    print "Welcome to the AIMS module"

def std(values):
    avg = np.mean(values)
    dev=[(i-avg)**2 for i in values]
    return np.sqrt(sum(dev)/len(values))

def avg_range(path):
    import numpy as np
    nums=[]
    for i in range(len(path)):
        data = open(path[i])
        for line in data:
            if line.startswith('Range'):
                 nums.append(int(line[-2]))
        return np.mean(nums)


if __name__=='__main__':
    main()
